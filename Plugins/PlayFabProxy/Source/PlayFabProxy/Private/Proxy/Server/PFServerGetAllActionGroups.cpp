// This is automatically generated by PlayFab SDKGenerator. DO NOT modify this manually!

#include "PFServerGetAllActionGroups.h"
#include "PlayFabProxy.h"
#include "PlayFab.h"

UPFServerGetAllActionGroups::UPFServerGetAllActionGroups(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, SuccessDelegate(PlayFab::UPlayFabServerAPI::FGetAllActionGroupsDelegate::CreateUObject(this, &ThisClass::OnSuccessCallback))
{
}

UPFServerGetAllActionGroups* UPFServerGetAllActionGroups::GetAllActionGroups(class APlayerController* PlayerController)
{
	UPFServerGetAllActionGroups* Proxy = NewObject<UPFServerGetAllActionGroups>();
	Proxy->PlayerControllerWeakPtr = PlayerController;
	
	return Proxy;
}

void UPFServerGetAllActionGroups::Activate()
{
	// grab the module, so we can get a valid pointer to the client API
	PlayFabServerPtr ServerAPI = IPlayFabModuleInterface::Get().GetServerAPI();

	bool CallResult = false;

	if(ServerAPI.IsValid())
	{
		CallResult = ServerAPI->GetAllActionGroups(SuccessDelegate, ErrorDelegate);
	}

	if(CallResult == false)
	{
		OnFailure.Broadcast();
	}
}

void UPFServerGetAllActionGroups::OnSuccessCallback(const PlayFab::ServerModels::FGetAllActionGroupsResult& Result)
{
	FBPServerGetAllActionGroupsResult BPResult;
 	BPResult.Data = Result;
 	OnSuccess.Broadcast(BPResult);
}
