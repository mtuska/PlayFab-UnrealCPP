// This is automatically generated by PlayFab SDKGenerator. DO NOT modify this manually!

#include "PFAdminResetUsers.h"
#include "PlayFabProxy.h"
#include "PlayFab.h"

UPFAdminResetUsers::UPFAdminResetUsers(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, SuccessDelegate(PlayFab::UPlayFabAdminAPI::FResetUsersDelegate::CreateUObject(this, &ThisClass::OnSuccessCallback))
{
}

UPFAdminResetUsers* UPFAdminResetUsers::ResetUsers(class APlayerController* PlayerController, const FBPAdminResetUsersRequest& InResetUsersRequest)
{
	UPFAdminResetUsers* Proxy = NewObject<UPFAdminResetUsers>();
	Proxy->PlayerControllerWeakPtr = PlayerController;
	Proxy->Request = InResetUsersRequest;
	
	return Proxy;
}

void UPFAdminResetUsers::Activate()
{
	// grab the module, so we can get a valid pointer to the client API
	PlayFabAdminPtr AdminAPI = IPlayFabModuleInterface::Get().GetAdminAPI();

	bool CallResult = false;

	if(AdminAPI.IsValid())
	{
		CallResult = AdminAPI->ResetUsers(Request.Data, SuccessDelegate, ErrorDelegate);
	}

	if(CallResult == false)
	{
		OnFailure.Broadcast();
	}
}

void UPFAdminResetUsers::OnSuccessCallback(const PlayFab::AdminModels::FBlankResult& Result)
{
	OnSuccess.Broadcast();
}
